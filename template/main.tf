terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "tls" {
  // no config needed
}

resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "ssh_private_key_pem" {
  content         = tls_private_key.ssh.private_key_pem
  filename        = ".ssh/google_compute_engine"
  file_permission = "0600"
}

data "tls_public_key" "ssh_public_key" {
  private_key_pem = "${local_file.ssh_private_key_pem.content}"
}

provider "google" {
  credentials = file("ai-pipeline-okos-b7275a0bbbd9.json")

  project = "ai-pipeline-okos"
  region  = var.region
  zone    = var.zone
}

resource "google_compute_network" "vpc_network" {
  name = "okos-network"
}

// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
 byte_length = 8
}

resource "google_compute_address" "static" {
  name = "ai-pipeline-vm"
  #deletion_protection = false
}

data "google_client_openid_userinfo" "me" {}

// A single Compute Engine instance
resource "google_compute_instance" "default" {
 name         = "flask-vm-${random_id.instance_id.hex}"
 machine_type = "f1-micro"
 zone         =  var.zone
 tags         = ["allow-ssh"]

<<<<<<< HEAD:main.tf
// metadata = {
////   ssh-keys = "admin:${file(var.publickeypath)}"
//   # ssh-keys = ["admin:${ file("${path.module}/server/certs/client_key.pub") }"]
//   # ssh-keys = "${split("@", data.google_client_openid_userinfo.me.email)[0]}:${tls_private_key.ssh.public_key_openssh}"
// }

// connection {
//      host        = google_compute_address.static.address
//      type        = "ssh"
//      user        = "admin"
//      timeout     = "10s"
//      agent       = false
//      private_key = "${file(var.privatekeypath)}"
//      #private_key = data.tls_public_key.ssh_public_key.private_key_pem
//    }
=======
 #metadata = {
 #  ssh-keys = "admin:${file(var.publickeypath)}"
   # ssh-keys = ["admin:${ file("${path.module}/server/certs/client_key.pub") }"]
   # ssh-keys = "${split("@", data.google_client_openid_userinfo.me.email)[0]}:${tls_private_key.ssh.public_key_openssh}"
 #}

 #connection {
 #     host        = google_compute_address.static.address
 #     type        = "ssh"
 #     user        = "admin"
 #     timeout     = "10s"
 #     agent       = false
 #     private_key = "${file(var.privatekeypath)}"
      #private_key = data.tls_public_key.ssh_public_key.private_key_pem
    #}
>>>>>>> 823aa1adcc58d56c1cc5dccc50fd010fc1006c5e:template/main.tf

 boot_disk {
   initialize_params {
     image = "debian-cloud/debian-9"
   }
 }

<<<<<<< HEAD:main.tf
// Make sure flask is installed on all new instances for later steps
//metadata_startup_script = <<EOT
//    sudo apt-get update;
//    sudo apt-get install -yq python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools;
//    sudo apt-get install python3-venv;
//
//    python3 -m venv app_env;
//    virtualenv app_env;
//    source app_env/bin/activate;
//
//    pip install wheel
//    pip3 install flask;
//    pip3 install gunicorn;
//
//    sudo ufw allow 5000
//
//    cd /etc/nginx/sites-enabled;
//    sudo chmod 755 /etc/systemd/system/app_service.service;
//    sudo ln -s /etc/nginx/sites-available/app;
//    sudo systemctl restart nginx;
//
//    sudo systemctl daemon-reload;
//    sudo systemctl enable app_service;
//    sudo systemctl start app_service;
//
//EOT

// provisioner "file" {
//    source      = "server/wsgi.py"
//    destination = "app/bin/wsgi.py"
//  }
//
// provisioner "file" {
//    source      = "server/app"
//    destination = "/etc/nginx/sites-available/app"
//  }
//
// provisioner "file" {
//    source      = "server/app"
//    destination = "/etc/systemd/system/app_service.service"
//  }

provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/script.sh",
      "/tmp/script.sh args"
=======
// Make sure flask is installed on all new instances for later steps'
/*
metadata_startup_script = <<EOT
    sudo apt-get update;
    sudo apt-get install -yq python3-pip python3-dev build-essential libssl-dev libffi-dev python3-setuptools;
    sudo apt-get install python3-venv;

    python3 -m venv app_env;
    virtualenv app_env;
    source app_env/bin/activate;

    pip install wheel
    pip3 install flask;
    pip3 install gunicorn;

    sudo ufw allow 5000

    cd /etc/nginx/sites-enabled;
    sudo chmod 755 /etc/systemd/system/app_service.service;
    sudo ln -s /etc/nginx/sites-available/app;
    sudo systemctl restart nginx;

    sudo systemctl daemon-reload;
    sudo systemctl enable app_service;
    sudo systemctl start app_service;

EOT
*/
 #provisioner "file" {
 #   source      = "server/wsgi.py"
 #   destination = "app/bin/wsgi.py"
 # }

 #provisioner "file" {
 #   source      = "server/app"
 #   destination = "/etc/nginx/sites-available/app"
 # }

 #provisioner "file" {
 #   source      = "server/app"
 #   destination = "/etc/systemd/system/app_service.service"
 # }

#provisioner "remote-exec" {
#    inline = [
#      "chmod +x /tmp/script.sh",
#      "/tmp/script.sh args"
>>>>>>> 823aa1adcc58d56c1cc5dccc50fd010fc1006c5e:template/main.tf
      # "sed -i 's/${orig}/${new}/g' /etc/systemd/system/app_service.service"
 #   ]
 # }

 network_interface {
   network = "default"

   access_config {
     nat_ip = "${google_compute_address.static.address}"
   }
 }

  #service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
  #  email  = google_service_account.default.email
  #  scopes = ["cloud-platform"]
  #}

}

resource "google_compute_firewall" "default" {
 name    = "flask-app-firewall"
 network = "default"
 target_tags   = ["allow-ssh"] // this targets our tagged VM
 source_ranges = ["0.0.0.0/0"]
 source_tags = ["mynetwork"]

 allow {
   protocol = "tcp"
   ports    = ["5000"]
 }

 allow {
   protocol = "tcp"
   ports    = ["22"]
 } 
}