terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
}

provider "tls" {
  // no config needed
}

resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "ssh_private_key_pem" {
  content         = tls_private_key.ssh.private_key_pem
  filename        = ".ssh/google_compute_engine"
  file_permission = "0600"
}

data "tls_public_key" "ssh_public_key" {
  private_key_pem = "${local_file.ssh_private_key_pem.content}"
}

data "template_file" "default" {
  template = file("${path.module}/deploy.sh")
  vars = {}
}

provider "google" {
  credentials = file("ai-pipeline-okos-b7275a0bbbd9.json")

  project = "ai-pipeline-okos"
  region  = var.region
  zone    = var.zone
}

//resource "google_compute_network" "vpc_network" {
//  name = "default"
//}

// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
 byte_length = 8
}

resource "google_compute_address" "static" {
  name = "ai-training-static"
  #deletion_protection = false
}

data "google_client_openid_userinfo" "me" {}

// A single Compute Engine instance
resource "google_compute_instance" "default" {
 name         = "flask-vm-${random_id.instance_id.hex}"
 machine_type = "a2-highgpu-1g"
 zone         =  var.zone
 tags         = ["allow-ssh"]

  #############################################################################
  # This is the 'local exec' method.
  # Ansible runs from the same host you run Terraform from
  #############################################################################
  provisioner "remote-exec" {
    inline = ["echo ${google_compute_instance.default.id}", "sudo apt update", "sudo apt install python3 -y", "echo Done!"]

    connection {
      host        = "${google_compute_address.static.address}"
      type        = "ssh"
      user        = "${var.ssh_user}"
      private_key = "${file(var.privatekeypath)}"

//      timeout = "1m"

    }
  }

  provisioner "local-exec" {
//    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -vvv -u milad -K --private-key ${var.privatekeypath} -e 'pub_key=${var.publickeypath}' /Users/milad/repos/okos-gcp-terraform/ai_trainer/ansible/playbook.yml"
//    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -vvv -u milad -K -i '${google_compute_address.static.address},' --private-key ${var.privatekeypath} -e 'pub_key=${var.publickeypath}, ansible_become_password=password' /Users/milad/repos/okos-gcp-terraform/ai_trainer/ansible/playbook.yml"
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -vvv -u root -i '${google_compute_address.static.address},' --private-key ${var.privatekeypath} -e 'pub_key=${var.publickeypath}' /Users/milad/repos/okos-gcp-terraform/ai_trainer/ansible/playbook.yml"
  }

  #############################################################################
  # This is the 'remote exec' method.
  # Ansible runs on the target host.
  #############################################################################

  # provisioner "remote-exec" {
  #   inline = [
  #     "mkdir /home/${var.ssh_user}/files",
  #     "mkdir /home/${var.ssh_user}/ansible",
  #   ]

  #   connection {
  #     type        = "ssh"
  #     user        = "${var.ssh_user}"
  #     private_key = "${file("${var.private_key_path}")}"
  #   }
  # }
  # provisioner "file" {
  #   source      = "../ansible/httpd.yml"
  #   destination = "/home/${var.ssh_user}/ansible/httpd.yml"

  #   connection {
  #     type        = "ssh"
  #     user        = "${var.ssh_user}"
  #     private_key = "${file("${var.private_key_path}")}"
  #   }
  # }
  # provisioner "file" {
  #   source      = "../files/index.j2"
  #   destination = "/home/${var.ssh_user}/files/index.j2"

  #   connection {
  #     type        = "ssh"
  #     user        = "${var.ssh_user}"
  #     private_key = "${file("${var.private_key_path}")}"
  #   }
  # }
  # provisioner "remote-exec" {
  #   inline = [
  #     "sudo yum -y install ansible",
  #     "cd ansible; ansible-playbook -c local -i \"localhost,\" httpd.yml",
  #   ]

  #   connection {
  #     type        = "ssh"
  #     user        = "${var.ssh_user}"
  #     private_key = "${file("${var.private_key_path}")}"
  #   }
  # }
  # Don't comment out this next line.
#}

 boot_disk {
   initialize_params {
     image = "debian-cloud/debian-9"
   }
 }

    metadata = {
      install-nvidia-driver= "True"
#      startup-script-url = "./deploy.sh"
      ssh-keys = "${var.ssh_user}:${file(var.publickeypath)}"
//      ssh-keys = ["admin:${ file("${path.module}/server/certs/client_key.pub") }"]
//      ssh-keys = "${split("@", data.google_client_openid_userinfo.me.email)[0]}:${tls_private_key.ssh.public_key_openssh}"


    }

  scheduling {
    on_host_maintenance = "TERMINATE"
  }

// guest_accelerator = [ {
//   count = 1
//   type = "nvidia-tesla-k80"
// }]

//metadata_startup_script = data.template_file.default

  network_interface {
     network = "default"

     access_config {
       nat_ip = "${google_compute_address.static.address}"
     }
   }
  }

#data "aws_eip" "my_instance_eip" {
#  public_ip = "35.215.58.81"
#}
#
#resource "aws_eip_association" "my_eip_association" {
#  instance_id   = aws_instance.my_instance.id
#  allocation_id = data.aws_eip.my_instance_eip.id
#}

resource "google_compute_firewall" "default" {
 name    = "ai-training-firewall"
 network = "default"
 target_tags   = ["allow-ssh"] // this targets our tagged VM
 source_ranges = ["0.0.0.0/0"]
 source_tags = ["mynetwork"]

 allow {
   protocol = "tcp"
   ports    = ["5000"]
 }

 allow {
   protocol = "tcp"
   ports    = ["22"]
 } 
}

