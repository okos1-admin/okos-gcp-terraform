//variable "ssh_user" {
//    type = string
//}
//
//variable "email" {
//    type = string
//}
//
//variable "project" {
//    type = string
//}

variable "region" {
    type = string
    default = "us-central1"
}

variable "zone" {
    type=string
    default= "us-central1-f"
}

variable "privatekeypath" {
    type = string
    default = "/Users/milad/.ssh/id_rsa"
}
variable "publickeypath" {
    type = string
    default = "/Users/milad/.ssh/id_rsa.pub"
}

variable "ssh_user" {
  description = "SSH user name to connect to your instance."
  default     = "milad"
}